# Smoke tests - Security

Smoke test for default selinux mode on provided running image.

## This Test Suite includes these tests

1. Confirm that SELinux is enabled and in enforcing mode by default.
[selinux_check.sh]

2. Confirm that there are no AVC SELinux errors after image is booted.
[selinux_avc_errors.sh]
