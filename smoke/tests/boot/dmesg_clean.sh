#!/bin/bash

#TODO: Workaround until the driver gets disabled at the kernel
ALLOWED_WARNING="Warning: Unmaintained driver is detected: nft_compat"

# Run once for debugging logs
dmesg -r -l crit,alert,emerg

# Count the number of lines
if [ "$(dmesg -r -l crit,alert,emerg | grep -v "${ALLOWED_WARNING}" -c)" -gt 0 ]; then
  >&2 echo "Errors in dmesg detected marked as critical, alert, or emergency"
  exit 1
fi
