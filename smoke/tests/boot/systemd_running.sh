#!/bin/bash

if ! systemctl is-system-running --wait ; then
  systemctl list-units --state=failed
  exit 1
fi
