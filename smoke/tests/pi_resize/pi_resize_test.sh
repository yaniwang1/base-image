#!/bin/bash

#Insertion of pi_resize string value in declared pi_resizerpm parameter.
pi_resizerpm="pi_resize"

#If pi_resize.rpm isn't installed on OS, then inform the user and exit.
if ! rpm -q --quiet "$pi_resizerpm" ; then
    #Exit code 1 for not installed pi_resize.rpm.
    echo "${pi_resizerpm} is not installed."
    exit 1
fi

#Insertion of pi_resize string value in declared pi_resizerpm parameter.
pi_resizerpm_version="$(rpm -q "$pi_resizerpm")"
echo "The pi_resize version: ${pi_resizerpm_version} is installed."

#Insert path to root partition in root_part parameter.
root_part="$(readlink -f /dev/disk/by-label/root)"

#Insert path to physical disk in path_to_disk parameter.
path_to_disk="$(readlink -f /dev/disk/by-diskseq/1)"

#Check physical disk size in Kbytes.
disk_size="$(fdisk -ls "$path_to_disk")"
echo "The disk size is: $disk_size KB."

#Check root partition size in Kbytes.
root_partition_size="$(fdisk -ls "$root_part")"
echo "The root partition size is: $root_partition_size KB."

#Calculate what % of the disk being used by root partition.
disk="$disk_size"
partition="$root_partition_size"

awk -v disk="$disk" -v partition="$partition" '
       BEGIN {
        printf "The root partition takes: %3.2f%% of the disk.\n", partition * 100 / disk
        exit
        }'

       echo -e "*Small deviation from 100% is expected.\n Its used by /boot and /boot/efi system partitions."

#Calculate number of last sectors remains.
#Subtract from number of total disk sectors the number of last sectors of the root partition.
LAST_SECTORS="$(fdisk -l "${root_part%p*}" | awk -v root_partition="$root_part" '/^Disk \// { disk_sectors=$7 } $1 == root_partition { last_partition_end=$3 } END { print disk_sectors - last_partition_end }')"

#Secondary GPT takes 34 sectors
#https://en.wikipedia.org/wiki/GUID_Partition_Table
SECONDARY_GPT=34

#Last remaining sectors have to be the same as secondary GPT.
if [ "$LAST_SECTORS" == "$SECONDARY_GPT" ]; then
	echo "The root partition is fully extended."

#Last remaining sectors aren't the same as secondary GPT.
else
	echo "The root partition is not fully extended."
fi

#Exit code 0 for installed pi_resize.rpm and 
#for successful root partition extension over the whole capacity of the disk.
exit 0
