# Smoke tests - Services

Smoke tests for services status on provided running image.

## This Test Suite includes these tests

1. Confirm that basic services are running (systemd, NetworkManager).

2. Confirm that podman service is not enabled on RHIVOS by default.
