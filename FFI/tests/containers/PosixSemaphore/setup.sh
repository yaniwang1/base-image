#!/bin/bash

# shellcheck source=SCRIPTDIR/../../common.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/../../common.sh

# setting up the container
cntr_builder="builder"
cntr_client="client"
test_image=$(build_container_image gcc)
file="tst_named_posix_semaphore"
run_test_container $cntr_client

if ! test -x tst_sys_named_posix_semaphore; then
  echo "Preparing test for system"
  gcc -o tst_sys_named_posix_semaphore -lrt named_posix_semaphore.c -Wall -Werror
fi

# compile named_posix_semaphore.c inside the builder container
if ! test -x "$file"; then
  echo "Preparing test for containers"
  run_test_container_from_image "$cntr_builder" "$test_image"
  echo "Compile '$file' in '$cntr_builder'"
  podman exec -it "$cntr_builder" gcc -o "$file" -lrt named_posix_semaphore.c -Wall -Werror
fi

# copying files
copy_script_to_container $file
echo "Copying '$file' file"
