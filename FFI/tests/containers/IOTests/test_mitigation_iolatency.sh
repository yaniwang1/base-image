#!/bin/bash
# shellcheck source=SCRIPTDIR/setup_iotests.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/setup_iotests.sh

# Run orderly without interference 
echo "Starting first run"
# shellcheck disable=SC2086
podman exec -it orderly ioping $IOPING_OPTS /test > run1.log

# Run confusion to start interference
echo "Starting interference using: $STRESS_CMD"

# shellcheck disable=SC2086
if test -n "$FFI_QM_SCENARIO"; then
    echo "Starting interference using: $STRESS_CMD_QM"
    mkdir -p /etc/containers/systemd/qm.container.d
    echo "[Container]" > /etc/containers/systemd/qm.container.d/interference.conf
    echo "PodmanArgs=--pids-limit=-1 --security-opt seccomp=/usr/share/qm/seccomp.json --security-opt label=nested --security-opt unmask=all $MITIGATION_OPTS" >> /etc/containers/systemd/qm.container.d/interference.conf
    reload_config
    sleep 5
    # shellcheck disable=SC2086
    podman exec -itd qm $STRESS_CMD_QM 
else
    echo "Starting interference using: $STRESS_CMD"
    # shellcheck disable=SC2086
    podman run --detach $MITIGATION_OPTS -v /var/test_confusion:/test:Z --workdir /test --name confusion "$test_image" $STRESS_CMD > /dev/null
fi

# Give stress-ng a moment to start
sleep 10

# Run orderly with interference
echo "Starting second run"
# shellcheck disable=SC2086
podman exec -it orderly ioping $IOPING_OPTS /test > run2.log

# Cleanup containers and directories
podman rm --force orderly > /dev/null
podman rm --force confusion > /dev/null
rm -rf /var/test_orderly
rm -rf /var/test_confusion
if test -n "$FFI_QM_SCENARIO"; then
    rm /etc/containers/systemd/qm.container.d/interference.conf
    reload_config
fi

# Process ioping output
avg1=$(cut -d ' ' -f6 < run1.log)
avg2=$(cut -d ' ' -f6 < run2.log)
mdev1=$(cut -d ' ' -f8 < run1.log)
mdev2=$(cut -d ' ' -f8 < run2.log)

# Sanity check
if [ "$mdev1" -gt "$avg1" ] || [ "$mdev2" -gt "$avg2" ]; then
  error "Standard deviation larger than average! Aborting.."
  exit 1
fi

# Interference presence check
max_rel_error=$(( avg1 * 10 ))
avg_difference=$(( avg1 - avg2 ))
avg_difference=${avg_difference#-}  # abs()
echo "First  run I/O latency is $((avg1 / 1000))us"
echo "Second run I/O latency is $((avg2 / 1000))us"
if [ "$avg_difference" -gt "$max_rel_error" ]; then
    error "Interference is present!"
    exit 1
else
    success "Interference is mitigated!"
fi

