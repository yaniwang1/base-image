#!/bin/bash

# shellcheck source=SCRIPTDIR/setup.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/setup.sh

printf "%s\n" "-- Running server container in ASIL-B with mount file volume"
podman exec "$cntr_mount_server" ./tst_asilb_socket_fd "$tst_name" "$f_name" 1 &

printf "%s\n" "-- Running client container in ASIL-B with mount file volume"
podman exec "$cntr_mount_client" ./tst_asilb_socket_fd "$tst_name" "$f_name" 1 > /dev/null