#!/bin/bash

# shellcheck source=SCRIPTDIR/setup.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/setup.sh

printf "%s\n" "-- Running server container in $BAD_CONTAINER"
# shellcheck disable=SC2086
podman exec -d $BAD_CONTAINER bash -c "podman run $CONTAINER_PARAMS --name $cntr_server $BASE_CONTAINER_IMAGE /var/tst_socket_fd $tst_name $f_name 0" > /dev/null

printf "%s\n" "-- Running client container in ASIL-B, Expect: Fail! Socket connection timeout."
# shellcheck disable=SC2086
podman exec $cntr_client ./tst_asilb_socket_fd $tst_name $f_name 0 > /dev/null