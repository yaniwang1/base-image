#!/bin/bash

# shellcheck source=SCRIPTDIR/../../common.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/../../common.sh

# Set the name of this test case and file.
tst_name="SharedFile"
f_name="hello"

# Define and run server and client containers.
cntr_server="server"
cntr_client="client"
cntr_mount_server="mount_server"
cntr_mount_client="mount_client"

# Prepare a text file
if ! test -e $f_name; then
   echo "Hello FFI test!" > ./$f_name
fi

run_test_container $cntr_server
run_test_container $cntr_client
# mount volume for hello file
run_test_container $cntr_mount_server "-v hello:/var/hello"
run_test_container $cntr_mount_client "-v hello:/var/hello"
test_image=$(build_container_image gcc)
cntr_builder="builder"
file="tst_socket_fd"

# Delete socket file
if [ -S $tst_name ]; then
   rm -f $tst_name
fi

if ! test -x tst_asilb_socket_fd; then
   gcc -o tst_asilb_socket_fd -lrt socket_fd.c -Wall -Werror
fi

if ! test -x "$file"; then
   run_test_container_from_image "$cntr_builder" "$test_image"
   echo "Compile '$file' in '$cntr_builder'"
   podman exec -it "$cntr_builder" gcc -o "$file" -lrt socket_fd.c -Wall -Werror
fi

# Copy tst_socket_fd and hello
copy_script_to_container tst_socket_fd
echo "Copying '$file' file"
copy_script_to_container $f_name
echo "Copying '$f_name' file"