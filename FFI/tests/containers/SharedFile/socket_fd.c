// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * Copyright Red Hat
 * Author: Dennis Brendel <dbrendel@redhat.com>
 *         weiwang <weiwang@redhat.com>
 */

#include <sys/socket.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <fcntl.h>
#include <sys/un.h>
#include <poll.h>
#include <stdlib.h>

#define BUFFSIZE 256

char * test_name;

// Send fd to socket
int send_fd(int socket, int fd)
{   
    struct msghdr msg = {0};
    char buf[CMSG_SPACE(sizeof(fd))];
    memset(buf, '\0', sizeof(buf));
    char iobuf[1];
    struct iovec io = {
        .iov_base = iobuf,
        .iov_len = sizeof(iobuf)
    };

    msg.msg_iov = &io;
    msg.msg_iovlen = 1;
    msg.msg_control = buf;
    msg.msg_controllen = sizeof(buf);

    struct cmsghdr * cmsg = CMSG_FIRSTHDR(&msg);
    cmsg->cmsg_level = SOL_SOCKET;
    cmsg->cmsg_type = SCM_RIGHTS;
    cmsg->cmsg_len = CMSG_LEN(sizeof(fd));

    *((int *) CMSG_DATA(cmsg)) = fd;
    msg.msg_controllen = CMSG_SPACE(sizeof(fd));

    if (sendmsg(socket, &msg, 0) < 0)
    {
        printf("FAIL:%s: Send file descriptor failed as %s", test_name, strerror(errno));
        return 1;
    }
    return 0;
}

// Receive fd from socket
int receive_fd(int socket)
{   
    struct msghdr msg = {0};

    char m_buf[BUFFSIZE];
    struct iovec io = {
        .iov_base = m_buf,
        .iov_len = sizeof(m_buf)
    };

    msg.msg_iov = &io;
    msg.msg_iovlen = 1;

    char c_buf[BUFFSIZE];
    msg.msg_control = c_buf;
    msg.msg_controllen = sizeof(c_buf);

    if (recvmsg(socket, &msg, 0) < 0)
    {
        printf("FAIL:%s: Receive file descriptor failed as %s \n", test_name, strerror(errno));
        return 1;
    }

    struct cmsghdr * cmsg = CMSG_FIRSTHDR(&msg);
    unsigned char * data = CMSG_DATA(cmsg);
    int fd = *((int *) data);
    return fd;
}

/*
If the file is unmounted, the file transferred via the socket is not readable. 
If the file is mounted, it is readable but not writable.
*/ 
// expected_result :
//           Non-zero ---> Read allowed
//           zero ---> Read not allowed
int test_read_access(int fd, int expected_result)
{
    printf("- - - Test read access - - -\n");
    char buf_read[BUFFSIZE] = { 0 };
    if (expected_result)
    {
        // Read allowed
        // If read() returns a value is no more than 0, it means that no file data has been read, return 1.
        if (read(fd, buf_read, sizeof(buf_read)) <= 0)
        {
            printf("FAIL:%s: Read mount volume is ALLOWED!!!\n", test_name);
            return 1;
        }
        return 0;
    }
    else
    {
        // Read not allowed
        // If the read() return value is greater than 0, it means that the file data has been read, return 1.
        if (read(fd, buf_read, sizeof(buf_read)) > 0)
        {
            printf("FAIL:%s: Read unmounted volume is FORBIDDEN!!!\n", test_name);
            return 1;
        }
        printf("PASS:%s: Read unmounted volume should fail\n", test_name);
        close(fd);
        return 0;
    }
}


// If the file is mounted, it isn't writable to a read-only file.
int test_write_access(int fd, int expected_result)
{
    if (!test_read_access(fd, expected_result))
    {
        printf("- - - Test write access - - -\n");
        char buf_write[BUFFSIZE] = { 0 };
        memset(buf_write, 1, sizeof(buf_write));

        // Write not allowed
        // If the write() return value is greater than 0, it means that the file data has been written, return 1.
        if (write(fd, buf_write, sizeof(buf_write)) > 0)
        {
                printf("FAIL:%s: Write to read-only file is FORBIDDEN!!!\n", test_name);
                return 1;
        }

        printf("PASS:%s: Write to read-only file should fail\n", test_name);
        close(fd);
        return 0;
    }
    return 1;
}

int main(int argc, char * argv[]) {
    int s, ss, timeout=5;
    unsigned int t;
    // This var is used to distinguish whether to run mount or not since they have different results.
    // Input it from podman execution command in cases.
    int mount;
    char * file_name;
    struct sockaddr_un client, server;

    if (argc < 3)
    {
        printf("FAIL:%s: Missing parameters!!! \n", test_name);
    }
      
    test_name = argv [1];
    file_name = argv[2];
    mount = atoi(argv[3]);

    int fd = open(file_name, O_RDONLY);
    if (fd < 0)
    {
        printf("FAIL:%s: Open file %s in read-only access mode failed as %s\n", test_name, file_name, strerror(errno));
        return 1;
    }

    s = socket(AF_UNIX, SOCK_STREAM, 0);
    if (s < 0)
    {
        printf("FAIL:%s: Create socket failed as %s! \n", test_name, strerror(errno));
        return 1;
    }

    client.sun_family = AF_UNIX;
    strcpy(client.sun_path, test_name);
    if (bind(s, (struct sockaddr *)&client, sizeof(client)) == -1) 
    {
        printf(" - - - Client start - - -\n");

        if (connect (s, (struct sockaddr *)&client, sizeof(client)) == -1)
        {
            printf("FAIL:%s: Connect to an opened socket failed as %s!\n", test_name, strerror(errno));
            return 1;
        } 
        else 
        {
            printf("- - - Send file - - -\n");
            int fd = open(file_name, O_RDONLY);
            if (fd < 0)
            {
                printf("FAIL:%s: Open file %s in read-only access mode failed as %s\n", test_name, file_name, strerror(errno));
                return 1;
            }
             
            send_fd(s, fd);
            close(fd);
            close(s);
        }
    }
    else
    {
        printf("- - - Server start - - -\n");
        if (listen(s, 5) == -1)
        {
            printf("FAIL:%s: Listen to the socket connections failed as %s\n", test_name, strerror(errno));
            return 1;
        }

        struct pollfd fds;
        fds.fd = s;
        fds.events = POLLIN;

        if (poll(&fds, 1, timeout * 1000) == 0) {
           fprintf(stderr, "Timeout!\n");
           close(s);
           return 1;
        }

        t = sizeof(server);
        if ((ss = accept(s, (struct sockaddr *)&server, &t)) == -1) {
            printf("FAIL:%s: Accept the socket connections failed as %s\n", test_name, strerror(errno));
            return 1;
        }
        int fd = receive_fd(ss);
        close(ss);

        // mount:
        //       zero --- unmount volume
        //       Non-zero --- mount volume
        if (mount == 0)
        {
            test_read_access(fd, mount);
        }
        else    
        {
           test_write_access(fd, mount);
        }
    }
} 
