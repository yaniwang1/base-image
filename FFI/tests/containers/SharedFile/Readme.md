# Freedom From Interference (FFI) - Shared socket/fd by mounting as a volume

This test set investigate a socket/file descriptor shared between two containers by mounting it as a volume.

## Test cases

All the test cases need these input parameters:

    $tst_name: "SharedFile"
    $f_name: "hello", and it's content is "Hello FFI test!"
    $mount: 1 or 0

1. File descriptor shared via socket between two containers in ASIL-B without mount as a volume

   Expected result:

        Compile 'tst_socket_fd' in 'builder'
        Copying 'tst_socket_fd' file
        Copying 'hello' file
        -- Running server container in ASIL-B without mount file volume
        -- Running client container in ASIL-B without mount file volume
        - - - Server start - - -
        - - - Test read access - - -
        FAIL:SharedFile: Read unmounted volume is FORBIDDEN!!!
        Terminating..
2. File descriptor shared via socket between two containers in ASIL-B mounting as a volume

   Expected result:

        Copying 'tst_socket_fd' file
        Copying 'hello' file
        -- Running server container in ASIL-B with mount file volume
        -- Running client container in ASIL-B with mount file volume
        - - - Client start - - -
        - - - Send file - - -
        Terminating..
3. File descriptor shared via socket between a ASIL-B container and a qm container without mount as a volume

   Expected result:

        -- Running server container in qm
        -- Running client container in ASIL-B, Expect: Fail! Socket connection timeout.
        Timeout!
        Caught signal! Exiting..
4. File descriptor shared via socket between a ASIL-B container and a qm container mounting as a volume

   Expected result:

        -- Running server container in qm
        -- Running client container in ASIL-B, Expect: Fail! Socket connection timeout.
        Timeout!
        Caught signal! Exiting..
5. File descriptor shared via socket between two containers in qm without mount as a volume

   Expected result:

        -- Running server container in qm
        -- Running client container in qm
        Terminating..
6. File descriptor shared via socket between two containers in qm mounting as a volume

   Expected result:

        -- Running server container in qm
        -- Running client container in qm
        Terminating..

Results location:

    output.txt.
