#!/bin/bash

# shellcheck source=SCRIPTDIR/setup.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/setup.sh

printf "%s\n" "-- Running server container in ASIL-B without mount file volume"
podman exec "$cntr_server" ./tst_asilb_socket_fd "$tst_name" "$f_name" 0 &

printf "%s\n" "-- Running client container in ASIL-B without mount file volume"
podman exec "$cntr_client" ./tst_asilb_socket_fd "$tst_name" "$f_name" 0 > /dev/null
