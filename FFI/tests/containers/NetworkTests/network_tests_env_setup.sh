#!/bin/bash

echo "Checking kernel-modules-extra package"
rpm -q --whatprovides kernel-modules-extra || dnf install -y kernel-modules-extra-"$(uname -r)"
echo "Build stress container image"
podman build -t stream9-stress -f "$(dirname "$0")"/Containerfile