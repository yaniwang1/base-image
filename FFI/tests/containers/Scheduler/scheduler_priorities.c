#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/resource.h>
#include <time.h>
#include "prime.h"


int main() {
    // Set priority of process
    if (setpriority(PRIO_PROCESS, getpid(), -20) != 0) {
        perror("setpriority failed");
        return EXIT_FAILURE;
    }

    // Record start time
    struct timespec start, end;
    clock_gettime(CLOCK_MONOTONIC, &start);

    // Run workload
    // Count primes up to 10 million
    int prime_count = count_primes();

    // Record end time
    clock_gettime(CLOCK_MONOTONIC, &end);

    // Measure the elapsed time
    double elapsed_time = (end.tv_sec - start.tv_sec) + (end.tv_nsec - start.tv_nsec) / 1e9;
    printf("setpriority(PRIO_PROCESS, getpid(), -20)\n");
    printf("Elapsed time for workload: %.6f seconds\n", elapsed_time);
    printf("Number of primes up to %d: %d\n", MAX_PRIME, prime_count);

    return EXIT_SUCCESS;
}

