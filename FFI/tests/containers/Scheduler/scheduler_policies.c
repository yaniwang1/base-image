#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sched.h>
#include <time.h>
#include "prime.h"

int main() {
    
    struct sched_param sch_param;
    int max_priority;
    int min_priority;

    // Processes scheduled under the SCHED_FIFO real-time policies have a sched_priority value in
    // the range 1 (low) to 99 (high). So the current process priority value 0 cannot be feed into
    // sched_setscheduler(). Also, we only have one process on the system with real time scheduling policy，
    // here choose (sched_get_priority_max - sched_get_priority_min) / 2 for sch_param.sched_priority
    max_priority = sched_get_priority_max(SCHED_FIFO);
    min_priority = sched_get_priority_min(SCHED_FIFO);
    sch_param.sched_priority = (max_priority - min_priority)/2;
    printf("sched_prioriy is: %d\n", sch_param.sched_priority);
    
    // Set the scheduler policy of the current thread to SCHED_FIFO
    if (sched_setscheduler(0, SCHED_FIFO, &sch_param) < 0) {
        perror("sched_setscheduler failed");
        return EXIT_FAILURE;
    }

    // Record start time
    struct timespec start, end;
    clock_gettime(CLOCK_MONOTONIC, &start);

    // Run the workload
    int prime_count = count_primes();
    
    // Record end time
    clock_gettime(CLOCK_MONOTONIC, &end);

    // Measure the elapsed time
    double elapsed_time = (end.tv_sec - start.tv_sec) + (end.tv_nsec - start.tv_nsec) / 1e9;
    printf("Scheduler policy of the current thread is SCHED_FIFO\n");
    printf("Elapsed time for workload: %f seconds\n", elapsed_time);
    printf("Number of primes up to %d: %d\n", MAX_PRIME, prime_count);

    return 0;
}

