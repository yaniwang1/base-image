#!/bin/bash

# shellcheck source=SCRIPTDIR/setup.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/setup.sh

printf "%s\n" "-- Running system_v_message_queues test in server container."
# shellcheck disable=SC2086
podman run $CONTAINER_PARAMS -d --name server "$BASE_CONTAINER_IMAGE" ./tst_system_v_message_queues > /dev/null

printf "%s\n" "-- Running system_v_message_queues test in client container. Expected: failure, not able to access the same queue."
# shellcheck disable=SC2086
podman run $CONTAINER_PARAMS --name client "$BASE_CONTAINER_IMAGE" ./tst_system_v_message_queues > /dev/null
