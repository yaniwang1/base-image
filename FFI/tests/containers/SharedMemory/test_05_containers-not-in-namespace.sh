#!/bin/bash

# shellcheck source=SCRIPTDIR/setup.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/setup.sh

printf "%s\n" "-- Running shm test in $cntr_orderly. Failure expected: Test aborted, timeout!"
podman exec "$cntr_orderly" ./tst_shm &

printf "%s\n" "-- Running shm test in $BAD_CONTAINER. Failure expected: Test aborted, timeout!"
# shellcheck disable=SC2154
podman exec "$BAD_CONTAINER" "$(get_path_to_script "$file")"
