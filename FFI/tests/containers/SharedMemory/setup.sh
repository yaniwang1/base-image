#!/bin/bash

# shellcheck source=SCRIPTDIR/../../common.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/../../common.sh

# setting up the containers
cntr_orderly="orderly"
cntr_ipc_host="bad_container_ipc_host_mode"
cntr_ipc_container="bad_container_ipc_container_mode"
cntr_builder="builder"
run_test_container $cntr_orderly
run_test_container $cntr_ipc_host "--ipc host"
run_test_container $cntr_ipc_container "--ipc container:$cntr_orderly"
test_image=$(build_container_image gcc)
file="tst_shm"

if ! test -x tst_sys_shm; then
  echo "Preparing test for system"
  gcc -o tst_sys_shm -lrt shm.c -Wall -Werror
fi

# compile shm.c inside the builder container
if ! test -x "$file"; then
  echo "Preparing test for containers"
  run_test_container_from_image "$cntr_builder" "$test_image"
  echo "Compile '$file' in '$cntr_builder'"
  podman exec -it "$cntr_builder" gcc -o "$file" -lrt shm.c -Wall -Werror
fi

# copying files
copy_script_to_container $file
echo "Copying '$file' file"
