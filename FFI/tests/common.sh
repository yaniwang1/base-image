#!/bin/bash

set -eE

if test ${#BASH_SOURCE[@]} -lt 2; then
    error "Do not call this directly!"
    exit 1
fi

RED='\033[0;31m'
GRN='\033[0;32m'

error() {
    __output 2 "$RED" "$*"
}

success() {
    __output 1 "$GRN" "$*"
}

__output() {
    fd="$1"
    color="$2"
    shift 2
    msg="$*"
    if test -t 1; then
        printf "$color%s\033[0;0m\n" "$msg" >&"$fd"
    else
        printf "%s\n" "$msg" >&"$fd"
    fi
}

__random() {
    shuf -ern 10 {a..z} | tr -d '\n'
}

# Override this function in the script to do additional cleanup on exit
__local_cleanup() {
    true
}

__cleanup() {
    __local_cleanup
    # TODO: test if that base image was there before

    cleanup_all_files
    
    if test -n "$__helpers"; then
        for helper in $__helpers; do
            if test "$helper" != "$BASE_CONTAINER_IMAGE"; then
                podman rmi -f "$helper"
            fi
        done
    fi

    if test ${#__run_cntr_helpers[@]} -ne 0; then
        for (( i=${#__run_cntr_helpers[@]} - 1; i >= 0; i--)) ; do
            # Attempt to stop the container
            if ! podman stop -t 0 -i "${__run_cntr_helpers[i]}"; then
                echo "Failed to stop container: ${__run_cntr_helpers[i]}" >&2
            fi
            # Always remove the container
            podman rm -fi "${__run_cntr_helpers[i]}"
        done
    fi

    # podman's main process exits sometimes before cleanup finished
    sleep 0.2
}

__exit_fail() {
    __cleanup
    exit 1
}

reload_config() {
   systemctl daemon-reload
   systemctl restart qm
}

build_container_image() {
    if test $# -lt 1; then
        __helper="$BASE_CONTAINER_IMAGE"
    else
        __requirements="$*"
        # TODO: Add a collision check?
        __helper=$(__random)
        cat <<EOF | podman build --quiet --tag "$__helper" -f - >/dev/null
FROM $BASE_CONTAINER_IMAGE

RUN dnf -y install $__requirements && dnf clean all

EOF
    fi

    __helpers+="$__helper "
    echo "$__helper"
}

# arg1: name of container
# arg2: output to wait for
# arg3: (optional) success message
# arg4: (optional) timeout
poll_container_for_string() {
    local container="$1"
    local search_string="$2"
    local message="Success"
    local timeout=60

    test -n "$3" && message="$3"
    test -n "$4" && timeout="$4"

    seconds_start=$(date +%s)
    while true; do
        seconds_end=$(date +%s)
        elapsed=$(( seconds_end - seconds_start ))
        if podman logs --tail 1 "$container" | grep -q "$search_string"; then
            echo "$message after ${elapsed}s"
            return 0
        fi
        if test $elapsed -ge "$timeout"; then
            return 1
        fi
        sleep 1
    done
}

# arg1: name of container
# arg2: (optional) podman run options
run_test_container() {
    local cntr_name=$1
    local run_options=""
    test -n "$2" && run_options=$2
    run_options=${run_options:-""}
    run_test_container_from_image "$cntr_name" "$BASE_CONTAINER_IMAGE" "$run_options"
}

# arg1: name of container
# arg2: name of container image
# arg3: (optional) podman run options
run_test_container_from_image() {
    local cntr_name=$1
    local cntr_img=$2
    local run_options=""
    test -n "$3" && run_options=$3
    run_options=${run_options:-""}

    # shellcheck disable=SC2086
    podman run --detach $CONTAINER_PARAMS --name $cntr_name $run_options $cntr_img
    __run_cntr_helpers+=( "$cntr_name" )
}

# arg1: name of the container
get_container_ip() {
    local cntr_name=$1
    podman container inspect -f '{{.NetworkSettings.IPAddress}}' "$cntr_name" 2>/dev/null
}

# arg1: name of the container
get_container_nic_name() {
    local cntr_name=$1
    local pid
    local netns
    pid=$(podman inspect "$cntr_name" --format '{{.State.Pid}}')
    netns=$(ip netns identify "$pid")
    ip link show | grep -B1 "$netns" | grep -Po '^\d*: \Kveth[0-9]*'
}

# arg1: script file
# arg2: path to the script file
get_path_to_script () {
    local file=$1
    local script_path="/var"
    
    if test -n "$FFI_QM_SCENARIO"; then
        echo "$script_path/$file"
    else
        echo ./"$file"
    fi
}

# global tracker for copied files
COPIED_FILES_TRACKER="/tmp/copied_files.txt"
true > "$COPIED_FILES_TRACKER"  # clear or create the tracker file

# arg1: script file
# arg2: path to the script file
copy_script_to_container() {
    local file=$1
    local script_path="/var"
    
    if test -n "$FFI_QM_SCENARIO"; then
        cp "$file" "$script_path"/qm/
    else
        podman cp "$file" "$BAD_CONTAINER":"$script_path"/tmp/"$file"
    fi

    # track the copied file for cleanup
    echo "$file" >> "$COPIED_FILES_TRACKER"
}

# function to delete copied files that were tracked
delete_script_from_container() {
    local file=$1
    local script_path="/var"

    if test -n "$FFI_QM_SCENARIO"; then
        rm -f "$script_path"/qm/"$file"
    else
        podman exec "$BAD_CONTAINER" rm -f "$script_path"/tmp/"$file"
    fi
}

# cleanup function to delete all tracked, copied files
cleanup_all_files() {
    if [[ -f "$COPIED_FILES_TRACKER" ]]; then
        echo "Starting cleanup..."
        while IFS= read -r file; do
            delete_script_from_container "$file"
        done < "$COPIED_FILES_TRACKER"
        rm -f "$COPIED_FILES_TRACKER"  # clean up the tracker file itself
        echo "Cleanup complete"
    else
        echo "Nothing to clean up. Terminating..."
    fi
}

install_package_in_bad_container() {
    local package=$1
    if test -n "$FFI_QM_SCENARIO"; then
        podman cp /etc/yum.repos.d qm:/etc/
        # shellcheck disable=SC2086
        dnf install --setopt=reposdir=/etc/qm/yum.repos.d --installroot=/usr/lib/qm/rootfs -y $package >/dev/null
    else
        podman exec "$BAD_CONTAINER" /bin/bash -c "dnf install -y $package" >/dev/null
    fi
}

wait_for_stress_ng_to_start() {
    local cpuload="$1"
    local cpu_num="$2"
    local retry=60
    local cpuload_expected=$((cpuload - 10))

    while [[ $retry -ge 1 ]]; do
        # shellcheck disable=SC2009
        loaded_cpu=$(ps aux | grep stress-ng | awk '{if ( $3 > "'$cpuload_expected'" ) print $3}' | wc -w)
        if [[ $loaded_cpu -eq $cpu_num ]]; then
            break;
        fi
        sleep 2
        (( retry-- ))
    done
    if [[ $retry -ge 1 ]]; then
        echo "stress-ng has been successfully started."
        sync
    else
        echo "stress-ng not started."
        exit 1
    fi
}

# Stop stress-ng to clean up the running container
stop_stress_ng() {
    echo "Stopping stress-ng, killed processes:"
    podman exec "$BAD_CONTAINER" pidof stress-ng && killall -9 stress-ng
    local retry=10
    while [[ $retry -ge 1 ]]; do
        if [[ $(pidof stress-ng | wc -w) -eq 0 ]]; then
            break;
        fi
        sleep 1
        (( retry-- ))
    done
    sync
}

chcon -R -t container_file_t .

CONTAINER_PARAMS="--rm -t -v $PWD:$PWD -w $PWD"

# prepare containers for testing
if test -n "$FFI_QM_SCENARIO"; then
    BAD_CONTAINER="qm"
else
    BAD_CONTAINER="bad_container"
    if test -n "$BAD_CONTAINER_IMAGE"; then
        run_test_container_from_image "$BAD_CONTAINER" "$BAD_CONTAINER_IMAGE" "$BAD_CONTAINER_OPTIONS"
    else
        run_test_container "$BAD_CONTAINER"
    fi
fi

trap "echo 'Caught signal! Exiting..'; __exit_fail" ERR SIGINT
trap "echo 'Terminating..'; __cleanup" 0

printf "%s\n" "-- Starting test ${BASH_SOURCE[1]}"
