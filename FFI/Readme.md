# Freedom From Interference (FFI)

With the ISO 26262 Freedom from interference (FFI) can be described as a system with a less critical ASIL
level which can not influence a system with a more critical ASIL application. The goal is to demonstrate
container technologies can be used to isolate applications belonging to different safety domains (ASIL-B and QM).

## To run the tests locally
1. `tmt run -a -vvv plans -n FFI`

## To run the tests inside a virtual machine:
1.  `tmt run -a -vvv plans -n FFI provision --how virtual --image centos-stream-9`

